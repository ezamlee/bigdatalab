# Big Data Lab

## Pre-request

- Login into Docker Playground and create 5 instances with all mangers from wrench icon.
- create a file called docker-compose.yml provided in the lab
- create a file hadoop-hive.env provided in the lab

---

## Objective

- Upload csv file and put it into hadoop Distributed file system using NiFi
- Load CSV in hive using CLI
- Load CSV File data into kafka
- Read Kafka Data using CLI
- Create Visualization using Superset and hive connection


## Preparation: Install Hadoop using the following commands

- `docker stack deploy -c ./docker-compose bigdata`
- to gain shell in the hadoop deployment
  - Search for the container: `docker stack ps bigdata` -> this should show on which manager the service is running under column NODE
  - on the manger the node is running run the following -> `docker container ls` and note down the ID
  - to gain shell into the container run ->  `docker exec -it [container id] /bin/bash`
- Extra
  - to copy files from host to container use docker cp [link](https://docs.docker.com/engine/reference/commandline/cp/)
  - to look at logs use command `docker service logs -f [service name]` you need to run this command on all services to make sure they are running smoothly.

## Tasks - 1

- Upload the following [CSV](USA-CRIME-DATA.csv) file to the Hadoop platfrom. 
- View the csv from the browser.

## Task - 2

- use NiFi HDFS processor to read file and put it into HDFS
- use NiFi to read csv file and put data into kafka
- Read Data from Kafka Again

### Task - 3

- Define Hive Table using external table and source from file.
- Query the csv data from hive using CLI

### Task - 4

- Load Hive Connection in superset
- Create Sample Dashboard on data
